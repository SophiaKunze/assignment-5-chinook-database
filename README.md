# Assignment 5: Chinook and Superheroes

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Java](https://img.shields.io/badge/-Java-red?logo=java)](https://www.java.com)
[![Spring](https://img.shields.io/badge/-Spring-white?logo=spring)](https://spring.io/)

This is a Spring Boot Application using a PostgreSQL database. This application is the second assignment
of the backend part of the [Noroff](https://www.noroff.no/en/) Full-Stack developer course.  
 
## Table of Contents

- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background
This assignment consists of two independent parts:
1. SQL scripts in directory [Appendix A](./appendix_A) to create a database.
2. An application for interaction with an existing database stored in this repository.

### SQL scripts to create and manipulate a database called superheroes 
Following steps are required to create the database and to perform the desired changes on the database:
- create tables
- add relationships between those tables
- insert data
- update data
- delete data

### A repository implementation to access and modify data from a provided database called chinook
Following functionality is covered in the implementation:
- Read all the customers in the database, this should display their: Id, first name, last name, country, postal code,
  phone number and email.
- Read a specific customer from the database (by Id), should display everything listed in the above point.
- Read a specific customer by name. HINT: LIKE keyword can help for partial matches.
- Return a page of customers from the database. This should take in limit and offset as parameters and make use
  of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above
  should be reused.
- Add a new customer to the database. You also need to add only the fields listed above (our customer object).
- Update an existing customer.
- Return the country with the most customers.
- Customer who is the highest spender (total in invoice table is the largest).
- For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context
  means the genre that corresponds to the most tracks from invoices associated to that customer.
  This query is stored [Appendix B](./appendix_B) for better clarity.

## Install

This project was generated with OpenJDK version 17.0.2 through Spring Initializr and Gradle build system. 
Clone repository via `git clone`.

## Maintainers

[@HuberMarkus](https://gitlab.com/hubermarkus)
[@SophiaKunze](https://gitlab.com/SophiaKunze)

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/SophiaKunze/assignment-5-chinook-database/-/issues/new). This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

### Acknowledgement
This project exists thanks to our teacher <a href="https://gitlab.com/NicholasLennox">Nicholas Lennox</a> and <a href=https://www.experis.de/de>Experis</a>.


## License

MIT © 2022 Sophia Kunze, Markus Huber
