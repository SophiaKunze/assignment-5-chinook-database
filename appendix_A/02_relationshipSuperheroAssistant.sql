ALTER TABLE assistant
    ADD COLUMN IF NOT EXISTS superhero_id int REFERENCES superhero;
