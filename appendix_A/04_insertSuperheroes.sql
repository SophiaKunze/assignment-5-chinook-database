/* Insert with columns specified */
INSERT INTO superhero (name, alias, origin)
VALUES ('Markus', 'Huaba', 'Altkirchen'),
       ('Sophia', 'Sophy', 'Seefeld'),
       ('Nicholas', 'Nick', 'South Africa');
