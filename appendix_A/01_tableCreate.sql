/* Ensures we dont try re-create tables */
DROP TABLE IF EXISTS superhero;
DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS power;

CREATE TABLE superhero (
    id serial PRIMARY KEY,
    name varchar(30) NOT NULL,
    alias varchar(30),
    origin varchar(50)
);

CREATE TABLE assistant (
    id serial PRIMARY KEY,
    name varchar(30)
);

CREATE TABLE power (
    id serial PRIMARY KEY,
    name varchar(30),
    description text
);