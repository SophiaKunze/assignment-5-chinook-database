INSERT INTO power (name, description)
VALUES ('programming', 'faster than marc zuckerberg'),
       ('testing', 'with full coverage'),
       ('designing user interfaces', 'with high accessibility'),
       ('katas', 'as fast as Kevin');

INSERT INTO superhero_power (superhero_id, power_id) VALUES (1,2), (2,3), (3,1), (3,2);