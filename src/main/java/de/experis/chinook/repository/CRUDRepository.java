package de.experis.chinook.repository;

import java.util.List;

public interface CRUDRepository<T, U> {
    /**
     * Reads all entries from database table with selected columns. The columns are displaying the fields of type T.
     *
     * @return all entries of a database table.
     */
    List<T> getAll();

    /**
     * Reads a database entry from a table by its id.
     *
     * @param id for selecting database entry.
     * @return the selected database entry.
     */
    T getById(U id);

    /**
     * Reads a subset/ page of entries from database table with selected columns. The columns are displaying the fields of type T.
     *
     * @param limit  of entries to be read.
     * @param offset defines the starting point.
     * @return selected entries of a database table.
     */
    List<T> getPage(int limit, int offset);

    /**
     * Adds a new entry to the database.
     *
     * @param object to be added to the database.
     * @return number of affected rows.
     */
    int insert(T object);

    /**
     * Update an existing database entry.
     *
     * @param object with updated information.
     * @return number of affected rows.
     */
    int update(T object);
}
