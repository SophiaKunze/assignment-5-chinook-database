package de.experis.chinook.repository;

/**
 * Database model specific constants.
 */
public class Constants {
    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String INVOICE_TABLE_NAME = "invoice";

}
