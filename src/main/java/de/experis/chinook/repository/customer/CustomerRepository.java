package de.experis.chinook.repository.customer;

import de.experis.chinook.model.Customer;
import de.experis.chinook.model.CustomerCountry;
import de.experis.chinook.model.CustomerGenre;
import de.experis.chinook.repository.CRUDRepository;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {
    /**
     * Read a specific customer by first name.
     *
     * @param name of customer to be read.
     * @return all matched customers.
     */
    List<Customer> getCustomerByName(String name);

    /**
     * Identifies the country with the most customers.
     *
     * @return country with most customers.
     */
    CustomerCountry getCountryMostCustomers();

    /**
     * Identifies the Customer who is the highest spender (total in invoice table is the largest).
     *
     * @return highest spender.
     */
    Customer getCustomerWithHighestSpending();

    /**
     * For a given customer, identifies its most popular genre(s) (in the case of a tie, all are displayed). Most popular in this context
     * means the genre that corresponds to the most tracks from invoices associated to that customer.
     *
     * @param customer for whom popular genres should be identified.
     * @return most popular genres.
     */
    List<CustomerGenre> getFavoriteGenres(Customer customer);
}
