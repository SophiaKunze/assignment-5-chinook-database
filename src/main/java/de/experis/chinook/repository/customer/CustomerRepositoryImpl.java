package de.experis.chinook.repository.customer;

import de.experis.chinook.model.Customer;
import de.experis.chinook.model.CustomerCountry;
import de.experis.chinook.model.CustomerGenre;
import de.experis.chinook.model.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static de.experis.chinook.repository.Constants.*;

// TODO README and License
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password
    ) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> getAll() {
        String sql = "SELECT * FROM " + CUSTOMER_TABLE_NAME;
        return getCustomers(sql);
    }


    @Override
    public Customer getById(Integer id) {
        String sql = "SELECT * FROM " + CUSTOMER_TABLE_NAME + " WHERE customer_id = " + id;
        Customer customer = null;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> getCustomerByName(String name) {
        String sql = "SELECT * FROM " +
                CUSTOMER_TABLE_NAME +
                " WHERE LOWER(first_name) LIKE LOWER('%" +
                name +
                "%')";
        return getCustomers(sql);
    }

    @Override
    public CustomerCountry getCountryMostCustomers() {
        String sql = "SELECT country, COUNT(*) AS country_count FROM customer GROUP BY country ORDER BY country_count DESC";
        CustomerCountry customerCountry = null;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            // access first row
            if (resultSet.next()) {
                customerCountry = new CustomerCountry(
                        resultSet.getString("country"),
                        resultSet.getInt("country_count"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerCountry;
    }

    @Override
    public Customer getCustomerWithHighestSpending() {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT customer_id, SUM(total) AS total_balance ");
        queryBuilder.append(" FROM ").append(INVOICE_TABLE_NAME);
        queryBuilder.append(" GROUP BY customer_id ORDER BY total_balance DESC");
        String sql = queryBuilder.toString();

        // 1. Step: Get customer_id(s) with the highest spending
        CustomerSpender highestSpender = null;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            // access first row
            if (resultSet.next()) {
                highestSpender = new CustomerSpender(
                        resultSet.getInt("customer_id"),
                        resultSet.getDouble("total_balance"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (highestSpender == null) {
            return null; // no CustomerSpender exists => Customer is also null
        }

        // 2. Step: Get customers corresponding to customer id(s) from (1.)
        return getById(highestSpender.id());
    }

    @Override
    public List<CustomerGenre> getFavoriteGenres(Customer customer) {
        String sql =
                "SELECT name\n" +
                "FROM (\n" +
                "         SELECT name, genre_count\n" +
                "         FROM (\n" +
                "                  -- group by genres\n" +
                "                  SELECT genre_id, COUNT(*) AS genre_count\n" +
                "                  FROM (\n" +
                "                           -- join to invoice_line_track table\n" +
                "                           SELECT genre_id\n" +
                "                           FROM (\n" +
                "                                    -- join to invoice_invoice_line table\n" +
                "                                    SELECT track_id\n" +
                "                                    FROM (\n" +
                "                                             -- invoices of selected customer\n" +
                "                                             SELECT invoice_id\n" +
                "                                             FROM invoice\n" +
                "                                             WHERE customer_id = ?\n" +
                "                                         )\n" +
                "                                             AS customer_invoice\n" +
                "                                             INNER JOIN invoice_line ON customer_invoice.invoice_id = invoice_line.invoice_id\n" +
                "                                )\n" +
                "                                    AS customer_genre\n" +
                "                                    INNER JOIN track ON customer_genre.track_id = track.track_id\n" +
                "                       )\n" +
                "                           AS customer_genre_id_count\n" +
                "                  GROUP BY genre_id\n" +
                "              )\n" +
                "                  AS customer_genre_name_count\n" +
                "                  INNER JOIN genre ON customer_genre_name_count.genre_id = genre.genre_id\n" +
                "         ORDER BY genre_count DESC\n" +
                "     ) AS customer_genre_name_count\n" +
                "WHERE genre_count =\n" +
                "      (SELECT MAX(genre_count)\n" +
                "       FROM (\n" +
                "                SELECT name, genre_count\n" +
                "                FROM (\n" +
                "                         -- group by genres\n" +
                "                         SELECT genre_id, COUNT(*) AS genre_count\n" +
                "                         FROM (\n" +
                "                                  -- join to invoice_line_track table\n" +
                "                                  SELECT genre_id\n" +
                "                                  FROM (\n" +
                "                                           -- join to invoice_invoice_line table\n" +
                "                                           SELECT track_id\n" +
                "                                           FROM (\n" +
                "                                                    -- invoices of selected customer\n" +
                "                                                    SELECT invoice_id\n" +
                "                                                    FROM invoice\n" +
                "                                                    WHERE customer_id = ?\n" +
                "                                                )\n" +
                "                                                    AS customer_invoice\n" +
                "                                                    INNER JOIN invoice_line ON customer_invoice.invoice_id = invoice_line.invoice_id\n" +
                "                                       )\n" +
                "                                           AS customer_genre\n" +
                "                                           INNER JOIN track ON customer_genre.track_id = track.track_id\n" +
                "                              )\n" +
                "                                  AS customer_genre_id_count\n" +
                "                         GROUP BY genre_id\n" +
                "                     )\n" +
                "                         AS customer_genre_name_count\n" +
                "                         INNER JOIN genre ON customer_genre_name_count.genre_id = genre.genre_id\n" +
                "                ORDER BY genre_count DESC\n" +
                "            ) AS max_count)";

        List<CustomerGenre> favoriteGenres = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, customer.id());
            preparedStatement.setInt(2, customer.id());
            ResultSet resultSet = preparedStatement.executeQuery();
            // access all ordered genres with counter
            while (resultSet.next()) {
                favoriteGenres.add(new CustomerGenre(customer.id(), resultSet.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return favoriteGenres;
    }

    @Override
    public List<Customer> getPage(int limit, int offset) {
        String sql = "SELECT * FROM " + CUSTOMER_TABLE_NAME + " LIMIT " + limit + " OFFSET " + offset;
        return getCustomers(sql);
    }

    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO " + CUSTOMER_TABLE_NAME + "(first_name, last_name, country, postal_code, phone, email)" + " VALUES (?, ?, ?, ?, ?, ?)";
        return executeCustomerUpdate(customer, sql);
    }

    @Override
    public int update(Customer customer) {
        String sql = "UPDATE " + CUSTOMER_TABLE_NAME + " SET " +
                "first_name" + " = ?, " +
                "last_name" + " = ?, " +
                "country" + " = ?, " +
                "postal_code" + " = ?, " +
                "phone" + " = ?, " +
                "email" + " = ? " +
                "WHERE customer_id = " + customer.id();

        return executeCustomerUpdate(customer, sql);

    }


    /**
     * Executes DML queries for a customer.
     *
     * @param customer to be modified.
     * @param sql      the query for modifying the customer.
     * @return number of affected rows.
     */
    private int executeCustomerUpdate(Customer customer, String sql) {
        int updatedRows = 0;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());

            updatedRows = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return updatedRows;
    }

    /**
     * Read customers from database, displaying their: Id, first name, last name, country, postal code, phone number and email.
     *
     * @param sql the query for selecting customers.
     * @return selected Customers.
     */
    private List<Customer> getCustomers(String sql) {
        List<Customer> customers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }
}
