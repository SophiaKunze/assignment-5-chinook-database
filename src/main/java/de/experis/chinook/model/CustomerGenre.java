package de.experis.chinook.model;

public record CustomerGenre(int customerId, String genre) {
}
