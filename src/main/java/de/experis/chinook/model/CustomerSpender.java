package de.experis.chinook.model;

public record CustomerSpender(int id, double totalSpendings) {
}
