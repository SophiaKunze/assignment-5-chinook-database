package de.experis.chinook.model;

public record Customer(int id, String firstName, String lastName, String country, String postalCode, String phoneNumber,
                       String email) {

    /**
     * Custom constructor for Customer's without initial ID.
     * Used, e.g., to create a Customer that should be added to the DB.
     *
     * @param firstName
     * @param lastName
     * @param country
     * @param postalCode
     * @param phoneNumber
     * @param email
     */
    public Customer(String firstName, String lastName, String country, String postalCode, String phoneNumber, String email) {
        this(-1, firstName, lastName, country, postalCode, phoneNumber, email);
    }
}
