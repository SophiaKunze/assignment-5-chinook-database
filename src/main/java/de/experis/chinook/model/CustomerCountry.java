package de.experis.chinook.model;

public record CustomerCountry(String country, int counter) {
}
