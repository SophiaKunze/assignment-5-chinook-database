package de.experis.chinook.runners;

import de.experis.chinook.model.Customer;
import de.experis.chinook.model.CustomerCountry;
import de.experis.chinook.model.CustomerGenre;
import de.experis.chinook.repository.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChinookAppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public ChinookAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        // requirement 1: Read all the customers in the database, this should display their: Id, first name, last name, country, postal code, phone number and email.
        List<Customer> allCustomer = this.customerRepository.getAll();
        // requirement 2: Read a specific customer from the database (by Id), should display everything listed in the above point.
        Customer customer = this.customerRepository.getById(12);
        // requirement 3: Read a specific customer by name. HINT: LIKE keyword can help for partial matches.
        List<Customer> customers = this.customerRepository.getCustomerByName("te");
        // requirement 4: Return a page of customers from the database. This should take in limit and offset as parameters and make use of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above should be reused.
        List<Customer> customerPage = this.customerRepository.getPage(20, 10);
        // requirement 5: Add a new customer to the database. You also need to add only the fields listed above (our customer object)
        Customer nick = new Customer("Nicholas", "Lennox", "SA", "348632", "11880", "nick-java-teacher@noroff.no");
        int numberInsertedUsers = this.customerRepository.insert(nick);
        // requirement 6: Update an existing customer. (get new identity for witness protection program)
        Customer angeloMerte = new Customer(5, "Angelo", "Merte", "German", "8573947", "+49 2873648", "Angelo.Merte@cancellor-germany.de");
        int numberUpdatedUsers = this.customerRepository.update(angeloMerte);
        // requirement 7: Return the country with the most customers.
        CustomerCountry countryMostCustomers = this.customerRepository.getCountryMostCustomers();
        // requirement 8: Customer who is the highest spender (total in invoice table is the largest).
        Customer highestSpender = this.customerRepository.getCustomerWithHighestSpending();
        // requirement 9: For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context
        //means the genre that corresponds to the most tracks from invoices associated to that customer.
        List<CustomerGenre> favoriteGenres = this.customerRepository.getFavoriteGenres(customer);
    }
}