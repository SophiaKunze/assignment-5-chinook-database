SELECT name
FROM (
         SELECT name, genre_count
         FROM (
                  -- group by genres
                  SELECT genre_id, COUNT(*) AS genre_count
                  FROM (
                           -- join to invoice_line_track table
                           SELECT genre_id
                           FROM (
                                    -- join to invoice_invoice_line table
                                    SELECT track_id
                                    FROM (
                                             -- invoices of selected customer
                                             SELECT invoice_id
                                             FROM invoice
                                             WHERE customer_id = 4 /* TODO replace with placeholder when needed*/
                                         )
                                             AS customer_invoice
                                             INNER JOIN invoice_line ON customer_invoice.invoice_id = invoice_line.invoice_id
                                )
                                    AS customer_genre
                                    INNER JOIN track ON customer_genre.track_id = track.track_id
                       )
                           AS customer_genre_id_count
                  GROUP BY genre_id
              )
                  AS customer_genre_name_count
                  INNER JOIN genre ON customer_genre_name_count.genre_id = genre.genre_id
         ORDER BY genre_count DESC
     ) AS customer_genre_name_count
WHERE genre_count =
      (SELECT MAX(genre_count)
       FROM (
                SELECT name, genre_count
                FROM (
                         -- group by genres
                         SELECT genre_id, COUNT(*) AS genre_count
                         FROM (
                                  -- join to invoice_line_track table
                                  SELECT genre_id
                                  FROM (
                                           -- join to invoice_invoice_line table
                                           SELECT track_id
                                           FROM (
                                                    -- invoices of selected customer
                                                    SELECT invoice_id
                                                    FROM invoice
                                                    WHERE customer_id = 4 /* TODO replace with placeholder when needed*/
                                                )
                                                    AS customer_invoice
                                                    INNER JOIN invoice_line ON customer_invoice.invoice_id = invoice_line.invoice_id
                                       )
                                           AS customer_genre
                                           INNER JOIN track ON customer_genre.track_id = track.track_id
                              )
                                  AS customer_genre_id_count
                         GROUP BY genre_id
                     )
                         AS customer_genre_name_count
                         INNER JOIN genre ON customer_genre_name_count.genre_id = genre.genre_id
                ORDER BY genre_count DESC
            ) AS max_count)